document.write('<script type="text/javascript" src="http://static.nycavp.webfactional.com/satchmo/js/dojo/dojo.js"></script>');
document.write('<script type="text/javascript">dojo.require("dojo.widget.Editor2");</script>');

var AddEditor = {
	init: function() {
		var helptext = document.getElementsByTagName('p');
		for (var i = 0, ht; ht = helptext[i]; i++) {
				if (ht.firstChild.data == "Rich Text Editing.") {
				    ht.previousSibling.previousSibling.setAttribute("dojoType", "Editor");
                                    // All features are enabled by default. For manual feature selection just uncomment next line.
				    //ht.previousSibling.previousSibling.setAttribute("items", "formatblock;|;insertunorderedlist;insertorderedlist;|;bold;italic;underline;strikethrough;|;createLink;");
				}
		}
	},
}

addEvent(window, 'load', AddEditor.init);