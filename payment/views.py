from satchmo.payment.common.views import common_contact

def contact_info(request):
    return common_contact.contact_info(request)
