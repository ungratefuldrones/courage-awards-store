from django.shortcuts import render_to_response
from django.conf import settings
from yahoo.search import web

def search(request):
	query = request.GET.get('q', None)
	site = ['www.avp.org']
	results = None
	if query:
		search = web.WebSearch(settings.YAHOO_ID, query=query, site=site)
		results = search.parse_results()
	return render_to_response('search.html', {'query': query, 'results': results})